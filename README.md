Welcome to marvelApp

This application was developed to list Marvel heroes in a list page of 10 results per page. When you click on a hero's name, you will be redirected to the hero's specific page where we have his image, description and the list with his comics.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />