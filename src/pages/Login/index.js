import React, { useState } from 'react';
import md5 from 'js-md5';
import api from '../../services/api';
import './styles.scss';

export default function Login({ history }) {
    const [privateKey, setPrivateKey] = useState('');
    const [publicKey, setPublicKey] = useState('');

    async function handleSubmit(e) {
        e.preventDefault();

        const hash = md5(`1${privateKey}${publicKey}`);
        try {
            await api.get(`characters?limit=1&ts=1&apikey=${publicKey}&hash=${hash}`);
            localStorage.setItem('hash', hash)
            localStorage.setItem('publicKey', publicKey)
            localStorage.setItem('initialPage', 0)
            history.push('/list');
        } catch (err) {
            alert("Acesso não autorizado");
        }
    }

    return (
        <div className="login-container">
            <form onSubmit={handleSubmit}>
                <label>Dados de acesso</label>
                <input type="text" placeholder="private_key" value={privateKey} onChange={e => setPrivateKey(e.target.value)} />
                <input type="text" placeholder="public_key" value={publicKey} onChange={e => setPublicKey(e.target.value)} />
                <button disabled={privateKey === '' || publicKey === ''}>Acessar</button>
            </form>
        </div>
    )
}