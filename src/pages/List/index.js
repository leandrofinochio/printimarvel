import React, { useEffect, useState } from 'react';
import './styles.scss';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import api from '../../services/api';
import loaderImg from '../../assets/loader.gif';

export default function List({ history }) {

    const publicKey = localStorage.getItem('publicKey');
    const hash = localStorage.getItem('hash');
    const initialPage = localStorage.getItem('initialPage');
    let [pageList, setPageList] = useState(parseInt(initialPage));
    let [maxView, setMaxView] = useState(10);
    const [totalEncontrados, setTotalEncontrados] = useState(0);
    const [rows, setRows] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        async function loadHeroes() {
            let offset = pageList * 10;
            const response = await api.get(`characters?limit=10&offset=${offset}&ts=1&apikey=${publicKey}&hash=${hash}`);
            response.data.data.results.forEach(row => {
                let date = new Date(row.modified)
                row.modified = formatDate(date)
            })
            setTotalEncontrados(response.data.data.total)
            setRows(response.data.data.results)
            setLoader(false)
        }

        function changeMaxView() {
            let compare = pageList * 10 + 10;

            if  (totalEncontrados > 0) {
                if (compare > totalEncontrados) {
                    setMaxView(maxView = totalEncontrados)
                } else {
                    setMaxView(maxView = compare)
                }
            } else {
                setMaxView(maxView = compare)
            }
        }

        loadHeroes();
        changeMaxView();
    }, [pageList])

    function handleSubmit(row) {
        localStorage.setItem('initialPage', pageList)
        history.push('/character', row)
    }

    function formatDate(date) {
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();

        month = month < 10 ? `0${month}` : month

        return `${day}/${month}/${year}`;
    }

    function previousPage() {
        setLoader(true)
        setPageList(pageList -= 1)
    }

    function nextPage() {
        setLoader(true)
        setPageList(pageList += 1)
    }

    function firstPage() {
        setLoader(true)
        setPageList(pageList = 0)
    }

    function lastPage() {
        setLoader(true)
        setPageList(pageList = parseInt(totalEncontrados / 10))
    }

    return (
        <div className="main">
            <div className="table">
                <TableContainer component={Paper}>
                    <Table aria-label="custom pagination table">
                        <TableHead>
                            <TableRow>
                                <TableCell>NOME</TableCell>
                                <TableCell>DESCRIÇÃO</TableCell>
                                <TableCell>ÚLTIMA ATUALIZAÇÃO</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {(!loader ?
                                (rows).map(row => (
                                    <TableRow key={row.name}>
                                        <TableCell component="th" scope="row" style={{width: '240px'}}>
                                            <p className="nomeTabela reticensias"  title={row.name} onClick={() => {handleSubmit(row)}}>{row.name}</p>
                                        </TableCell>
                                        <TableCell className="descricaoTabela">
                                            <p>{row.description}</p>
                                        </TableCell>
                                        <TableCell style={{width: '190px'}}>{row.modified}</TableCell>
                                    </TableRow>
                                ))
                                :
                                <img src={loaderImg} alt="Loading" className="loaderList" />
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
            <div className="div">
                <p> <b>{pageList * 10 + 1}</b> - <b>{maxView}</b> of <b>{totalEncontrados}</b> </p>
                <button type="button" onClick={() => firstPage()} disabled={maxView === 10}>Primeira</button>
                <button type="button" onClick={() => previousPage()} disabled={maxView === 10}>Anterior</button>
                <button type="button" onClick={() => nextPage()} disabled={maxView === totalEncontrados}>Próxima</button>
                <button type="button" onClick={() => lastPage()} disabled={maxView === totalEncontrados}>Última</button>
            </div>
        </div>
    )
}