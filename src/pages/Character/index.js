import React, { useState, useEffect } from 'react';
import api from '../../services/api';
import './styles.scss';
import loaderImg from '../../assets/loader.gif';

export default function Character({ history }) {
    const hero = history.location.state;
    const [character, setCharacter] = useState({});
    const [pathImage, setPathImage] = useState({});
    const [loader, setLoader] = useState(true);
    const [comics, setComics] = useState([]);
    const publicKey = localStorage.getItem('publicKey');
    const hash = localStorage.getItem('hash');

    if (!hero) {
        history.push('/list');
    }

    useEffect(() => {
        async function loadCharacter() {
            const response = await api.get(`characters/${hero.id}?ts=1&apikey=${publicKey}&hash=${hash}`);
            setCharacter(response.data.data.results[0])
            setPathImage(`${response.data.data.results[0].thumbnail.path}.${response.data.data.results[0].thumbnail.extension}`)
        }

        loadCharacter();
    }, [hero])

    useEffect(() => {
        async function loadComics() {
            const response = await api.get(`characters/${hero.id}/comics?ts=1&apikey=${publicKey}&hash=${hash}`);
            response.data.data.results.forEach(comic => {
                comic.pathImage = `${comic.thumbnail.path}.${comic.thumbnail.extension}`
            })
            setComics(response.data.data.results)
            setLoader(false)
        }

        loadComics();
    }, [hero])

    function handleSubmit() {
        history.push('/list');
    }

    return (
        <>
            <div className="character-container">
                <div className="character">
                    <img src={pathImage} alt="Hero" />
                    <div>
                        <strong>{character.name}</strong>
                        <textarea placeholder="Descrição" value={character.description} readOnly></textarea>
                    </div>
                    <button onClick={() => {handleSubmit()}}>Voltar</button>
                </div>
                <label>Fascículos</label>
                {(!loader ? 
                    <div>
                        {(comics.length > 0 ? (comics).map(comic => (
                            <div className="comics" key={comic.id}>
                                <img src={comic.pathImage} alt="" />
                                <div className="infosComic">
                                    <div>
                                        <div>
                                            <strong>Título:</strong>
                                            <p>{comic.title}</p>
                                        </div>
                                        <div>
                                            <strong>Número da capa:</strong>
                                            <p>{comic.issueNumber}</p>
                                        </div>
                                    </div>
                                    <textarea placeholder="Descrição da história" value={comic.description ? comic.description : ''}  readOnly></textarea>
                                </div>
                            </div>
                        ))
                        :
                        <h3 className="loader">Nenhum fascículo entrado</h3>
                        )}
                    </div>
                    :
                    <div className="loader">
                        <img src={loaderImg} alt="Loading" />
                    </div>
                )}
            </div>
        </>
    )
}