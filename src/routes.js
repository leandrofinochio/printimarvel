import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Character from './pages/Character';
import List from './pages/List';
import Login from './pages/Login';

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/character" component={Character} />
                <Route path="/list" component={List} />
                <Route path="/" exact component={Login} />
                <Route path="/login" component={Login} />
            </Switch>
        </BrowserRouter>
    );
}